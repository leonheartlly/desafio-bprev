package com.example.demo.service;

import com.example.demo.domain.Categoria;
import com.example.demo.dto.CategoriaDTO;
import com.example.demo.repository.CategoriaRepository;
import com.example.demo.service.Exceptions.DataIntegrityException;
import com.example.demo.service.Exceptions.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class CategoriaService {


    @Autowired
    private CategoriaRepository categoriaRepository;


    public List<Categoria> listAll() {

        List< Categoria > categorias = categoriaRepository.findAll();

        return categorias;
    }

    public Categoria findById(Long id) {

        Optional<Categoria> categoria = categoriaRepository.findById(id);

        return categoria.orElseThrow(
                () -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + id + ", Tipo: " + Categoria.class.getName())
        );
    }

    @Transactional
    public Categoria insert(Categoria categoria) {

        categoria.setId(null);

        return categoriaRepository.save(categoria);
    }

    public void delete(Long id) {

        try {
            findById(id);

            categoriaRepository.deleteById(id);
        }catch (DataIntegrityViolationException dive) {
            log.error(dive);
            throw new DataIntegrityException("Não foi possível efetuar a remoção.");
        }
    }


    public Categoria update(Categoria categoria) {

        Categoria old = findById(categoria.getId());

        updateData(old, categoria);
        return categoriaRepository.save(old);
    }

    private void updateData( Categoria oldCategoria, Categoria newCategoria ) {

        oldCategoria.setCategoria( newCategoria.getCategoria() );
    }




    public Categoria convertDTO(CategoriaDTO categoriaDTO) {

        Categoria categoria = new Categoria();
        categoria.setCategoria(categoriaDTO.getCategoria());

        return categoria;
    }

}
