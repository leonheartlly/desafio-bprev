package com.example.demo.service.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Constraint(validatedBy = PedidoItemValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PedidoItem {

    String message() default "Validation Error";


    Class<?>[] groups() default {};


    Class<? extends Payload>[] payload() default {};

}
