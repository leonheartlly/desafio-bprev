package com.example.demo.service.validator;


import com.example.demo.domain.Pedido;
import com.example.demo.domain.PedidoItemPK;
import com.example.demo.domain.Produto;
import com.example.demo.dto.PedidoItemDTO;
import com.example.demo.repository.PedidoRepository;
import com.example.demo.repository.ProdutoRepository;
import com.example.demo.resources.exceptions.FieldMessage;
import com.example.demo.service.PedidoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
public class PedidoItemValidator implements ConstraintValidator<PedidoItem, PedidoItemDTO> {

    @Autowired
    private PedidoService pedidoService;

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private PedidoRepository pedidoRepository;


    @Override
    public void initialize(PedidoItem ann) {

    }

    /**
     * Custom validations to casino and operator.
     *
     * @param dto     new product contact.
     * @param context
     * @return is valid.
     */
    @Override
    public boolean isValid(PedidoItemDTO dto, ConstraintValidatorContext context) {

        List<FieldMessage> invalidList = new ArrayList<>();

        PedidoItemPK pk = new PedidoItemPK();
        if (dto.getProdutoId() != null && dto.getProdutoId() > 0) {
            Optional<Produto> produto = produtoRepository.findById(dto.getProdutoId());
            if (produto.isPresent()) {
                pk.setProduto(produto.get());
            } else {
                log.warn("[VALIDACAO] Produto inexistente." + dto);
                invalidList.add(new FieldMessage("ProdutoId", "Pedido inexistente." + dto.getPedidoId()));
            }

        } else {
            log.warn("[VALIDACAO] Produto inváido." + dto);
            invalidList.add(new FieldMessage("produtoId", "Produto inválido." + dto.getProdutoId()));
        }


        if (dto.getPedidoId() != null && dto.getPedidoId() > 0) {
            Optional<Pedido> pedido = pedidoRepository.findById(dto.getPedidoId());
            if (pedido.isPresent()) {
                pk.setPedido(pedido.get());
            } else {
                log.warn("[VALIDACAO] Pedido inexistente." + dto);
                invalidList.add(new FieldMessage("pedidoId", "Pedido inexistente." + dto.getPedidoId()));
            }
        } else {
            log.warn("[VALIDACAO] Pedido inváido." + dto);
            invalidList.add(new FieldMessage("pedidoId", "Pedido inválido." + dto.getPedidoId()));
        }

        for (FieldMessage e : invalidList) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName()).addConstraintViolation();
        }

        return invalidList.isEmpty();
    }
}
