package com.example.demo.service;

import com.example.demo.domain.Pedido;
import com.example.demo.domain.enuns.PedidoStatus;
import com.example.demo.dto.PedidoDTO;
import com.example.demo.repository.PedidoRepository;
import com.example.demo.service.Exceptions.DataIntegrityException;
import com.example.demo.service.Exceptions.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class PedidoService {


    @Autowired
    private PedidoRepository pedidoRepository;


    public List<Pedido> listAll() {

        List<Pedido> pedidos = pedidoRepository.findAll();

        return pedidos;
    }

    public List<PedidoStatus> listAllStatus() {

        List<PedidoStatus> pedidos = Arrays.asList(PedidoStatus.values());

        return pedidos;
    }

    public Pedido findById(Long id) {

        Optional<Pedido> pedido = pedidoRepository.findById(id);

        return pedido.orElseThrow(
                () -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + id + ", Tipo: " + Pedido.class.getSimpleName())
        );
    }

    @Transactional
    public Pedido insert(Pedido pedido) {

        pedido.setId(null);
        pedido.setData(new Date());

        return pedidoRepository.save(pedido);
    }

    public void delete(Long id) {

        try {
            findById(id);

            pedidoRepository.deleteById(id);
        } catch (DataIntegrityViolationException dive) {
            log.error(dive);
            throw new DataIntegrityException("Não foi possível efetuar a remoção.");
        }
    }

    @Transactional
    public Pedido update(Pedido pedido) {

        Pedido old = findById(pedido.getId());

        updateData(old, pedido);
        return pedidoRepository.save(old);
    }

    private void updateData(Pedido oldPedido, Pedido newPedido) {

        oldPedido.setData(new Date());
        oldPedido.setSessao(newPedido.getSessao());
        oldPedido.setStatus(newPedido.getStatus());
    }

    public Pedido convertDTO(PedidoDTO pedidoDTO) {

        Pedido pedido = new Pedido();

        pedido.setStatus(pedidoDTO.getStatus());
        pedido.setData(pedidoDTO.getData());
        pedido.setSessao(pedidoDTO.getSessao());
        return pedido;
    }

}
