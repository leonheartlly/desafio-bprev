package com.example.demo.service;

import com.example.demo.domain.Categoria;
import com.example.demo.domain.Produto;
import com.example.demo.dto.ProdutoDTO;
import com.example.demo.repository.CategoriaRepository;
import com.example.demo.repository.ProdutoRepository;
import com.example.demo.service.Exceptions.DataIntegrityException;
import com.example.demo.service.Exceptions.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class ProdutoService {


    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private CategoriaRepository categoriaRepository;


    public List<Produto> listAll() {

        List<Produto> produtos = produtoRepository.findAll();

        return produtos;
    }

    public Produto findById(Long id) {

        Optional<Produto> produto = produtoRepository.findById(id);

        return produto.orElseThrow(
                () -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + id + ", Tipo: " + Produto.class.getName())
        );
    }

    @Transactional
    public Produto insert(Produto produto) {

        produto.setId(null);

        return produtoRepository.save(produto);
    }

    public void delete(Long id) {

        try {
            findById(id);

            produtoRepository.deleteById(id);
        } catch (ObjectNotFoundException onfe) {
            log.error(onfe);
        } catch (DataIntegrityViolationException dive) {
            log.error(dive);
            throw new DataIntegrityException("Não foi possível efetuar a remoção.");
        }
    }


    public Produto update(Produto pedido) throws ObjectNotFoundException {

        Produto old = findById(pedido.getId());

        updateData(old, pedido);
        return produtoRepository.save(old);
    }

    private void updateData(Produto oldProduto, Produto newProduto) {

        oldProduto.setDescricao(newProduto.getDescricao());
        oldProduto.setPreco(newProduto.getPreco());
        oldProduto.setProduto(newProduto.getProduto());
        oldProduto.setQuantidade(newProduto.getQuantidade());
        oldProduto.setFoto(newProduto.getFoto());
        oldProduto.setCategoriaId(newProduto.getCategoriaId());
    }

    public Produto convertDTO(ProdutoDTO produtoDTO) throws ObjectNotFoundException {

        Produto produto = new Produto();

        verificaCategoria(produtoDTO.getIdCategoria());
        produto.setCategoriaId(produtoDTO.getIdCategoria());

        produto.setFoto(produtoDTO.getFoto());
        produto.setQuantidade(produtoDTO.getQuantidade());
        produto.setDescricao(produtoDTO.getDescricao());
        produto.setProduto(produtoDTO.getProduto());
        produto.setPreco(produtoDTO.getPreco());


        return produto;
    }

    private void verificaCategoria(Long idCategoria) {

        if (idCategoria != null && idCategoria > 0) {
            Optional<Categoria> categoria = categoriaRepository.findById(idCategoria);

            log.info("Categoria encontrada: " + categoria.get().getCategoria());

        } else {
            log.warn(
                    "[VALIDACAO] Categoria informada é inválida. ID Categoria: " + idCategoria);
            throw new ObjectNotFoundException("Categoria informada é inválida.");
        }

    }

}
