package com.example.demo.service;

import com.example.demo.domain.Cliente;
import com.example.demo.dto.ClienteDTO;
import com.example.demo.repository.ClienteRepository;
import com.example.demo.service.Exceptions.DataIntegrityException;
import com.example.demo.service.Exceptions.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class ClienteService {


    @Autowired
    private ClienteRepository clienteRepository;


    public List<Cliente> listAll() {

        List<Cliente> clientes = clienteRepository.findAll();

        return clientes;
    }


    public Cliente findById(Long id) {

        Optional<Cliente> cliente = clienteRepository.findById(id);

        return cliente.orElseThrow(
                () -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + id + ", Tipo: " + Cliente.class.getName())
        );
    }

    @Transactional
    public Cliente insert(Cliente casino) {

        casino.setId(null);

        return clienteRepository.save(casino);
    }

    public void delete(Long id) {

        try {
            findById(id);

            clienteRepository.deleteById(id);
        } catch (ObjectNotFoundException onfe) {
            log.error(onfe);
        } catch (DataIntegrityViolationException dive) {
            log.error(dive);
            throw new DataIntegrityException("Não foi possível efetuar a remoção.");
        }
    }


    public Cliente update(Cliente cliente) {

        Cliente old = findById(cliente.getId());

        updateData(old, cliente);
        return clienteRepository.save(old);
    }

    private void updateData(Cliente oldCliente, Cliente newCliente) {

        oldCliente.setSenha(newCliente.getSenha());
        oldCliente.setRua(newCliente.getRua());
        oldCliente.setCidade(newCliente.getCidade());
        oldCliente.setEstado(newCliente.getEstado());
        oldCliente.setEmail(newCliente.getEmail());
        oldCliente.setCep(newCliente.getCep());
        oldCliente.setBairro(newCliente.getBairro());
        oldCliente.setNome(newCliente.getNome());
    }


    public Cliente convertDTO(ClienteDTO casinoDTO) {

        Cliente cliente = new Cliente();

        cliente.setNome(casinoDTO.getNome());
        cliente.setEmail(casinoDTO.getEmail());
        cliente.setSenha(casinoDTO.getSenha());
        cliente.setBairro(casinoDTO.getBairro());
        cliente.setCep(casinoDTO.getCep());
        cliente.setEstado(casinoDTO.getEstado());
        cliente.setCidade(casinoDTO.getCidade());
        cliente.setRua(casinoDTO.getRua());
        return cliente;
    }


}
