package com.example.demo.service;

import com.example.demo.domain.Pedido;
import com.example.demo.domain.PedidoItem;
import com.example.demo.domain.PedidoItemPK;
import com.example.demo.domain.Produto;
import com.example.demo.dto.PedidoItemDTO;
import com.example.demo.dto.PedidoItemKey;
import com.example.demo.repository.PedidoItemRepository;
import com.example.demo.repository.PedidoRepository;
import com.example.demo.repository.ProdutoRepository;
import com.example.demo.service.Exceptions.DataIntegrityException;
import com.example.demo.service.Exceptions.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class PedidoItemService {


    @Autowired
    private PedidoItemRepository pedidoItemRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private PedidoRepository pedidoRepository;


    public List<PedidoItem> listAll() {

        List<PedidoItem> pedidos = pedidoItemRepository.findAll();

        return pedidos;
    }

    public PedidoItem findById(PedidoItemKey id) {

        PedidoItemPK pk = this.checkItemId(id);
        Optional<PedidoItem> item = pedidoItemRepository.findById(pk);

        return item.orElseThrow(
                () -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + pk + ", Tipo: " + PedidoItem.class.getName())
        );
    }

    @Transactional
    public PedidoItem insert(PedidoItem item) {


        PedidoItemPK pk = checkItemId(new PedidoItemDTO(item));
        item.setId(pk);

        return pedidoItemRepository.save(item);
    }

    public void delete(PedidoItemKey id) {

        try {
            findById(id);

            PedidoItemPK pk = this.checkItemId(id);
            pedidoItemRepository.deleteById(pk);
        } catch (DataIntegrityViolationException dive) {
            log.error(dive);
            throw new DataIntegrityException("Não foi possível efetuar a remoção.");
        }
    }

    @Transactional
    public PedidoItem update(PedidoItem item) {

        //PedidoItem old = findById(checkItemId(new PedidoItemDTO(item)));

        // updateData(old, item);
        //return pedidoItemRepository.save(old);
        return null;
    }

    private void updateData(PedidoItem oldPedido, PedidoItem newPedido) {

        oldPedido.setQuantidade(newPedido.getQuantidade());
        oldPedido.setSubtotal(newPedido.getSubtotal());
        oldPedido.setValor(newPedido.getValor());
    }

    public PedidoItemPK checkItemId(PedidoItemDTO dto) {


        if (dto != null && dto.getPedidoId() != null && dto.getProdutoId() != null) {
            PedidoItemPK pk = new PedidoItemPK();

            Optional<Produto> produto = produtoRepository.findById(dto.getProdutoId());
            pk.setProduto(produto.orElseThrow(() -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + dto.getProdutoId() + ", Tipo: " + Produto.class.getSimpleName())));


            Optional<Pedido> pedido = pedidoRepository.findById(dto.getPedidoId());
            pk.setPedido(pedido.orElseThrow(() -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + dto.getPedidoId() + ", Tipo: " + Pedido.class.getSimpleName())));

            return pk;
        } else {
            log.warn(
                    "[VALIDACAO] Pedido ou produto informado é inválido. " + dto);
            throw new ObjectNotFoundException("Item solicitado nao encontrado.");
        }
    }

    public PedidoItemPK checkItemId(PedidoItemKey key) {


        if (key != null && key.getProdutoId() != null && key.getPedidoId() != null) {
            PedidoItemPK pk = new PedidoItemPK();

            Optional<Produto> produto = produtoRepository.findById(key.getProdutoId());
            pk.setProduto(produto.orElseThrow(() -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + key.getProdutoId() + ", Tipo: " + Produto.class.getSimpleName())));


            Optional<Pedido> pedido = pedidoRepository.findById(key.getPedidoId());
            pk.setPedido(pedido.orElseThrow(() -> new ObjectNotFoundException("O objeto solicitado não foi encontrado. Id: " + key.getPedidoId() + ", Tipo: " + Pedido.class.getSimpleName())));

            return pk;
        } else {
            log.warn(
                    "[VALIDACAO] Pedido ou produto informado é inválido. ");
            throw new ObjectNotFoundException("Item solicitado nao encontrado.");
        }
    }

    public PedidoItem convertDTO(PedidoItemDTO itemDTO) {

        PedidoItem item = new PedidoItem();

        item.setId(checkItemId(itemDTO));
        item.setValor(itemDTO.getValor());
        item.setQuantidade(itemDTO.getQuantidade());
        item.setSubtotal(itemDTO.getSubtotal());
        return item;
    }

}
