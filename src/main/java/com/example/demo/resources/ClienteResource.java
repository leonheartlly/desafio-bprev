package com.example.demo.resources;


import com.example.demo.domain.Cliente;
import com.example.demo.dto.ClienteDTO;
import com.example.demo.service.ClienteService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(value = "/desafio/clientes")
public class ClienteResource {


    @Autowired
    private ClienteService clienteService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ClienteDTO>> listAll() {

        List<Cliente> clientes = clienteService.listAll();
        List<ClienteDTO> clienteDTOS = clientes.stream().map(obj -> new ClienteDTO(obj)).collect(Collectors.toList());

        return ResponseEntity.ok().body(clienteDTOS);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ClienteDTO> findById(@PathVariable Long id) {

        Cliente cliente = clienteService.findById(id);
        ClienteDTO clienteDTO = new ClienteDTO(cliente);

        return ResponseEntity.ok().body(clienteDTO);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody ClienteDTO clienteDTO) {

        clienteDTO.setId(null);
        Cliente casino = clienteService.convertDTO(clienteDTO);

        casino = clienteService.insert(casino);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(casino.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody ClienteDTO clienteDTO, @PathVariable Long id) {

        Cliente cliente = clienteService.convertDTO(clienteDTO);
        cliente.setId(id);

        cliente = clienteService.update(cliente);

        return ResponseEntity.noContent().build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {

        clienteService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
