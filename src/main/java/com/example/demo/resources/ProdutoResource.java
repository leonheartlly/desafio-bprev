package com.example.demo.resources;


import com.example.demo.domain.Produto;
import com.example.demo.dto.ProdutoDTO;
import com.example.demo.service.ProdutoService;
import javassist.tools.rmi.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(value = "/desafio/produtos")
public class ProdutoResource {


    @Autowired
    private ProdutoService produtoService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ProdutoDTO>> listAll() {

        List<Produto> produtos = produtoService.listAll();
        List<ProdutoDTO> ProdutoDTOS = produtos.stream().map(obj -> new ProdutoDTO(obj)).collect(Collectors.toList());

        return ResponseEntity.ok().body(ProdutoDTOS);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProdutoDTO> findById(@PathVariable Long id) throws ObjectNotFoundException {

        Produto produto = produtoService.findById(id);
        ProdutoDTO produtoDTO = new ProdutoDTO(produto);

        return ResponseEntity.ok().body(produtoDTO);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody ProdutoDTO produtoDTO) {

        produtoDTO.setId(null);
        Produto pedido = produtoService.convertDTO(produtoDTO);

        pedido = produtoService.insert(pedido);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(pedido.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody ProdutoDTO produtoDTO, @PathVariable Long id) throws ObjectNotFoundException {

        Produto pedido = produtoService.convertDTO(produtoDTO);
        pedido.setId(id);

        pedido = produtoService.update(pedido);

        return ResponseEntity.noContent().build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {

        produtoService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
