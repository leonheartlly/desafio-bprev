package com.example.demo.resources;


import com.example.demo.domain.PedidoItem;
import com.example.demo.dto.PedidoItemDTO;
import com.example.demo.dto.PedidoItemKey;
import com.example.demo.service.PedidoItemService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(value = "/desafio/itens-pedido")
public class PedidoItemResource {


    @Autowired
    private PedidoItemService pedidoItemService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PedidoItemDTO>> listAll() {

        List<PedidoItem> pedidos = pedidoItemService.listAll();
        List<PedidoItemDTO> pedidoDTOS = pedidos.stream().map(obj -> new PedidoItemDTO(obj)).collect(Collectors.toList());

        return ResponseEntity.ok().body(pedidoDTOS);
    }

    @RequestMapping(value = "/{pedidoId}/{produtoId}", method = RequestMethod.GET)
    public ResponseEntity<PedidoItemDTO> findById(@PathVariable Long pedidoId, Long produtoId) {

        PedidoItem item = pedidoItemService.findById(new PedidoItemKey(pedidoId, produtoId));
        PedidoItemDTO pedidoDTO = new PedidoItemDTO(item);

        return ResponseEntity.ok().body(pedidoDTO);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody PedidoItemDTO itemDTO) {

        itemDTO.setId(null);

        PedidoItem item = pedidoItemService.convertDTO(itemDTO);
        item = pedidoItemService.insert(item);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(item.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody PedidoItemDTO itemDTO) {

        PedidoItem item = pedidoItemService.convertDTO(itemDTO);
        pedidoItemService.checkItemId(itemDTO);

        item = pedidoItemService.update(item);

        return ResponseEntity.noContent().build();
    }


    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@Valid @RequestBody PedidoItemKey id) {

        pedidoItemService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
