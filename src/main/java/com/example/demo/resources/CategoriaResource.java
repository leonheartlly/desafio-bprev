package com.example.demo.resources;


import com.example.demo.domain.Categoria;
import com.example.demo.dto.CategoriaDTO;
import com.example.demo.service.CategoriaService;
import javassist.tools.rmi.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(value = "/desafio/categorias")
public class CategoriaResource {


    @Autowired
    private CategoriaService categoriaService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<CategoriaDTO>> listAll() {

        List<Categoria> categorias = categoriaService.listAll();
        List<CategoriaDTO> categoriaDTOS = categorias.stream().map(obj -> new CategoriaDTO(obj)).collect(Collectors.toList());

        return ResponseEntity.ok().body(categoriaDTOS);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CategoriaDTO> findById(@PathVariable Long id) throws ObjectNotFoundException {

        Categoria categoria = categoriaService.findById(id);
        CategoriaDTO CategoriaDTO = new CategoriaDTO(categoria);

        return ResponseEntity.ok().body(CategoriaDTO);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody CategoriaDTO categoriaDTO) {

        categoriaDTO.setId(null);
        Categoria categoria = categoriaService.convertDTO(categoriaDTO);

        categoria = categoriaService.insert(categoria);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(categoria.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody CategoriaDTO categoriaDTO, @PathVariable Long id) throws ObjectNotFoundException {

        Categoria categoria = categoriaService.convertDTO(categoriaDTO);
        categoria.setId(id);

        categoria = categoriaService.update(categoria);

        return ResponseEntity.noContent().build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {

        categoriaService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
