package com.example.demo.resources;


import com.example.demo.domain.Pedido;
import com.example.demo.domain.enuns.PedidoStatus;
import com.example.demo.dto.PedidoDTO;
import com.example.demo.service.PedidoService;
import javassist.tools.rmi.ObjectNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping(value = "/desafio/pedidos")
public class PedidoResource {


    @Autowired
    private PedidoService pedidoService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PedidoDTO>> listAll() {

        List<Pedido> pedidos = pedidoService.listAll();
        List<PedidoDTO> pedidoDTOS = pedidos.stream().map(obj -> new PedidoDTO(obj)).collect(Collectors.toList());

        return ResponseEntity.ok().body(pedidoDTOS);
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public ResponseEntity<List<PedidoStatus>> listAllStatus() {

        List<PedidoStatus> pedidos = pedidoService.listAllStatus();

        return ResponseEntity.ok().body(pedidos);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<PedidoDTO> findById(@PathVariable Long id) throws ObjectNotFoundException {

        Pedido pedido = pedidoService.findById(id);
        PedidoDTO pedidoDTO = new PedidoDTO(pedido);

        return ResponseEntity.ok().body(pedidoDTO);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> add(@Valid @RequestBody PedidoDTO pedidoDTO) {

        pedidoDTO.setId(null);


        Pedido pedido = pedidoService.convertDTO(pedidoDTO);
        pedido = pedidoService.insert(pedido);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(pedido.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody PedidoDTO pedidoDTO, @PathVariable Long id) throws ObjectNotFoundException {

        Pedido pedido = pedidoService.convertDTO(pedidoDTO);
        pedido.setId(id);

        pedido = pedidoService.update(pedido);

        return ResponseEntity.noContent().build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {

        pedidoService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
