package com.example.demo.domain;

import com.example.demo.domain.enuns.PedidoStatus;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Pedido  implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;


    private Integer status;

    @Column( length = 50, nullable = false  )
    private String sessao;

    private Date data;


    @OneToMany( mappedBy = "id.pedido" )
    private Set< PedidoItem > itens = new HashSet<>();



    public void setStatus(PedidoStatus pedidoStatus ) {

        this.status = pedidoStatus.getCode();
    }


    public PedidoStatus getStatus() {

        return PedidoStatus.getPedidoStatus( status );
    }

}
