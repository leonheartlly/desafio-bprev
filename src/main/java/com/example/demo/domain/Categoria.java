package com.example.demo.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
public class Categoria  implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 20, nullable = false )
    private String categoria;


    public Categoria(Long id, String categoria) {
        this.id = id;
        this.categoria = categoria;
    }

    public Categoria() {
    }
}
