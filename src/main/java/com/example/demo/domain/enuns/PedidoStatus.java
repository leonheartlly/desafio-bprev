package com.example.demo.domain.enuns;


import java.util.Optional;


public enum PedidoStatus {

    PROCESSANDO( 1, "PROCESSANDO" ),
    CANCELADO( 2, "CANCELADO" ),
    CONCLUIDO(3, "CONCLUIDO"),
    BLOQUEADO( 4, "BLOQUEADO" );

    private int code;

    private String desc;


    PedidoStatus(int code, String desc ) {

        this.code = code;
        this.desc = desc;
    }


    public int getCode() {

        return code;
    }


    public String getDesc() {

        return desc;
    }


    public static PedidoStatus getPedidoStatus(Integer cod ) {

        if ( !Optional.ofNullable( cod ).isPresent() ) {
            return null;
        }

        for ( PedidoStatus ps : PedidoStatus.values() ) {
            if ( cod.equals( ps.getCode() ) ) {
                return ps;
            }
        }
        throw new IllegalArgumentException( "Id inválido: " + cod );
    }

}
