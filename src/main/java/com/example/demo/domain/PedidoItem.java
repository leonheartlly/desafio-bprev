package com.example.demo.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class PedidoItem implements Serializable {

    @JsonIgnore
    @EmbeddedId
    private PedidoItemPK id = new PedidoItemPK();

    private Integer quantidade;

    private Double valor;

    private Double subtotal;


    public Pedido getPedido(){
        return this.id.getPedido();
    }

    public Produto getProduto(){
        return this.id.getProduto();
    }

    public void setProduto(Produto produto){
        this.id.setProduto(produto);
    }

    public void setPedido(Pedido pedido){
        this.id.setPedido(pedido);
    }


    public PedidoItemPK getId() {
        return id;
    }

    public void setId(PedidoItemPK id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }
}
