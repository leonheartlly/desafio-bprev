package com.example.demo.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
public class Cliente  implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( length = 50, nullable = false )
    private String nome;

    @Column( length = 25, nullable = false )
    private String senha;

    @Column( length = 35 )
    private String email;

    @Column( length = 50 )
    private String rua;

    @Column( length = 30 )
    private String cidade;

    @Column( length = 30 )
    private String bairro;

    @Column( length = 10 )
    private String cep;

    @Column( length = 30 )
    private String estado;
}
