package com.example.demo.domain;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
public class PedidoItemPK implements Serializable {

    @ManyToOne
    @JoinColumn(name = "produto_id")
    private Produto produto;

    @ManyToOne
    @JoinColumn(name = "pedido_id")
    private Pedido pedido;


    public PedidoItemPK(Produto produto, Pedido pedido) {
        this.produto = produto;
        this.pedido = pedido;
    }

    public PedidoItemPK() {
    }



}
