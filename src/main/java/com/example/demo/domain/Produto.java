package com.example.demo.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
public class Produto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50)
    private String produto;

    private Double preco;

    private Long quantidade;

    @Column(length = 200)
    private String descricao;

    @Column(length = 250)
    private String foto;

    private Long categoriaId;

    @JsonIgnore
    @OneToMany(mappedBy = "id.produto")
    private Set<PedidoItem> itens = new HashSet<>();


    public Produto() {
    }

    public Produto(Long id, String produto, Double preco, Long quantidade, String descricao, String foto, Long categoriaId, Set<PedidoItem> itens) {
        this.id = id;
        this.produto = produto;
        this.preco = preco;
        this.quantidade = quantidade;
        this.descricao = descricao;
        this.foto = foto;
        this.categoriaId = categoriaId;
        this.itens = itens;
    }


    @JsonIgnore
    public List<Pedido> getPedidos() {

        List<Pedido> list = new ArrayList<>();
        for (PedidoItem oi : itens) {
            list.add(oi.getPedido());
        }
        return list;
    }


}
