package com.example.demo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class PedidoItemKey implements Serializable {

    @NotNull
    private Long pedidoId;

    @NotNull
    private Long produtoId;


    public PedidoItemKey(@NotNull Long pedidoId, @NotNull Long produtoId) {
        this.pedidoId = pedidoId;
        this.produtoId = produtoId;
    }

    public PedidoItemKey() {
    }
}
