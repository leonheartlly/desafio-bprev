package com.example.demo.dto;


import com.example.demo.domain.Cliente;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.lang.Nullable;

import javax.validation.constraints.*;

@Data
public class ClienteDTO {

    @JsonIgnore
    @Nullable
    private Long id;

    @NotEmpty( message = "Nome não pode ser vazio." )
    private String nome;

    @NotEmpty( message = "Senha não pode ser vazio." )
    @NotNull 
    private String senha;

    @NotEmpty( message = "Email não pode ser vazio." )
    private String email;

    @NotEmpty( message = "Rua não pode ser vazio." )
    private String rua;

    @NotEmpty( message = "Cidade não pode ser vazio." )
    private String cidade;

    @NotEmpty( message = "Bairro não pode ser vazio." )
    private String bairro;

    @NotNull
    private String cep;

    @NotEmpty( message = "Estado não pode ser vazio." )
    private String estado;


    public ClienteDTO(Cliente cliente) {
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.senha = cliente.getSenha();
        this.email = cliente.getEmail();
        this.rua = cliente.getRua();
        this.cidade = cliente.getCidade();
        this.bairro = cliente.getBairro();
        this.cep = cliente.getCep();
        this.estado = cliente.getEstado();
    }

    public ClienteDTO() {
    }
}
