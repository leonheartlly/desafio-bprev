package com.example.demo.dto;


import com.example.demo.domain.Produto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ProdutoDTO {

    @JsonIgnore
    private Long id;

    @NotEmpty( message = "Produto não pode ser vazio." )
    @NotNull
    private String produto;

    @NotNull
    private Double preco;

    private Long quantidade;

    @NotEmpty( message = "Descrição não pode ser vazio." )
    @NotNull
    private String descricao;

    @NotEmpty( message = "URL da imagem não pode ser vazia." )
    @NotNull
    private String foto;

    private Long idCategoria;

    public ProdutoDTO() {
    }

    public ProdutoDTO(Produto produto) {
        this.produto = produto.getProduto();
        this.preco = produto.getPreco();
        this.quantidade = produto.getQuantidade();
        this.descricao = produto.getDescricao();
        this.foto = produto.getFoto();
        this.idCategoria = produto.getCategoriaId();
    }
}
