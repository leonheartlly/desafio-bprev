package com.example.demo.dto;


import com.example.demo.domain.Categoria;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CategoriaDTO {

    @JsonIgnore
    private Long id;

    @NotEmpty( message = "Categoria não pode ser vazia." )
    @NotNull
    private String categoria;

    public CategoriaDTO(Categoria categoria) {
        this.categoria = categoria.getCategoria();
    }

    public CategoriaDTO() {
    }
}
