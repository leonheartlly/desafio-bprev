package com.example.demo.dto;


import com.example.demo.domain.Pedido;
import com.example.demo.domain.enuns.PedidoStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class PedidoDTO {

    @JsonIgnore
    private Long id;

    @NotNull
    private PedidoStatus status;

    @NotEmpty( message = "Sessão não pode ser vazia." )
    @NotNull
    private String sessao;

    @Nullable
    private Date data;

    public PedidoDTO() {
    }

    public PedidoDTO(Pedido pedido) {
        this.status = pedido.getStatus();
        this.sessao = pedido.getSessao();
        this.data = pedido.getData();
    }
}
