package com.example.demo.dto;


import com.example.demo.domain.PedidoItem;
import com.example.demo.domain.PedidoItemPK;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@com.example.demo.service.validator.PedidoItem
public class PedidoItemDTO implements Serializable {

    @JsonIgnore
    @EmbeddedId
    private PedidoItemPK id = new PedidoItemPK();

    @Min(value=1, message="Quantidade não deve ser menor que 1.")
    @Max(value=1000, message="É impossível você ter tantos itens.")
    private Integer quantidade;

    @Min(value=0, message="Não desejamos doar nenhum produto.")
    private Double valor;

    private Double subtotal;

    @NotNull
    @Min(value=1, message="Não existe pedido 0.")
    private Long pedidoId;

    @NotNull
    @Min(value=1, message="Não existe produto 0.")
    private Long produtoId;


    public PedidoItemDTO() {
    }

    public PedidoItemDTO(PedidoItem item) {
        this.id = item.getId();
        this.quantidade = item.getQuantidade();
        this.valor = item.getValor();
        this.subtotal = item.getSubtotal();
        this.pedidoId = item.getId().getPedido().getId();
        this.produtoId = item.getId().getProduto().getId();
    }


    @Override
    public String toString() {
        return "PedidoItemDTO{" +
                "quantidade=" + quantidade +
                ", valor=" + valor +
                ", pedidoId=" + pedidoId +
                ", produtoId=" + produtoId +
                '}';
    }
}
