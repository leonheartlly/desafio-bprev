package com.example.demo.repository;

import com.example.demo.domain.PedidoItem;
import com.example.demo.domain.PedidoItemPK;
import com.example.demo.dto.PedidoItemKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PedidoItemRepository extends JpaRepository<PedidoItem, PedidoItemPK> {

}
