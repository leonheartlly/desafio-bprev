
CREATE TABLE desafio.categoria (
  id int(11) NOT NULL AUTO_INCREMENT,
  categoria varchar(20) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET @@global.time_zone = '-3:00';