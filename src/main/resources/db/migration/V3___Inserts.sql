INSERT INTO desafio.categoria (id, categoria) VALUES ('1', 'enlatado');
INSERT INTO desafio.categoria (id, categoria) VALUES ('2', 'fruta');
INSERT INTO desafio.categoria (id, categoria) VALUES ('3', 'verdura');
INSERT INTO desafio.categoria (id, categoria) VALUES ('4', 'carne bovino');


ALTER TABLE desafio.categoria
ADD UNIQUE INDEX id_UNIQUE (id ASC);

ALTER TABLE desafio.cliente
CHANGE COLUMN cep cep VARCHAR(10) NOT NULL ;

INSERT INTO desafio.cliente (id, bairro, cep, cidade, email, estado, nome, rua, senha) VALUES ('3', 'Jerusalém', '04648001', 'Texas', 'mails@outlook.com', 'PA', 'Clodoaldo', 'R. Tiloca do grilo', '465');


ALTER TABLE `desafio`.`produto`
ADD COLUMN `categoria_id` INT(11) NOT NULL AFTER `quantidade`,
ADD INDEX `fk_prodxcat_idx` (`categoria_id` ASC);
;
ALTER TABLE `desafio`.`produto`
ADD CONSTRAINT `fk_prodxcat`
  FOREIGN KEY (`categoria_id`)
  REFERENCES `desafio`.`categoria` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


INSERT INTO `desafio`.`produto` (`id`, `descricao`, `foto`, `preco`, `produto`, `quantidade`, `categoria_id`) VALUES ('1', 'Milho a vapor', 'https://images.tcdn.com.br/img/img_prod/551663/milho_verde_ao_vapor_lata_200g_fugini_94_1_20180910092857.jpg', '2.50', 'Milho Crocante', '80', '1');
INSERT INTO `desafio`.`produto` (`id`, `descricao`, `foto`, `preco`, `produto`, `quantidade`, `categoria_id`) VALUES ('2', 'Mamão do seu zé, ruim como sempre', 'https://i.ytimg.com/vi/C3Z1Hnh_OrY/hqdefault.jpg', '8.00', 'Mamão Papaya', '13', '2');

INSERT INTO `desafio`.`pedido` (`id`, `data`, `sessao`, `status`) VALUES ('1', '2019-12-27 23:28:10', 'sessaosessaosessaosessaosessaosessao', '1');
INSERT INTO `desafio`.`pedido` (`id`, `data`, `sessao`, `status`) VALUES ('2', '2019-12-27 23:28:10', 'sessaosessaosessaosessaosessaosessao', '2');
