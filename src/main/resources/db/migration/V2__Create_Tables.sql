
CREATE TABLE desafio.cliente (
  id int(11) NOT NULL AUTO_INCREMENT,
  bairro varchar(30) DEFAULT NULL,
  cep int(11) NOT NULL,
  cidade varchar(30) DEFAULT NULL,
  email varchar(35) DEFAULT NULL,
  estado varchar(30) DEFAULT NULL,
  nome varchar(50) NOT NULL,
  rua varchar(50) DEFAULT NULL,
  senha varchar(25) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE desafio.pedido (
  id int(11) NOT NULL AUTO_INCREMENT,
  data datetime DEFAULT NULL,
  sessao varchar(50) DEFAULT NULL,
  status int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE desafio.produto (
  id int(11) NOT NULL AUTO_INCREMENT,
  descricao varchar(200) DEFAULT NULL,
  foto varchar(250) DEFAULT NULL,
  preco varchar(12) DEFAULT NULL,
  produto varchar(50) DEFAULT NULL,
  quantidade int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE desafio.pedido_item (
  quantidade int(11) DEFAULT NULL,
  subtotal double DEFAULT NULL,
  valor double DEFAULT NULL,
  pedido_id int(11) NOT NULL,
  produto_id int(11) NOT NULL,
  PRIMARY KEY (pedido_id,produto_id),
  KEY FK8eyfr31j751fjws2y012awmpg (produto_id),
  CONSTRAINT FK8eyfr31j751fjws2y012awmpg FOREIGN KEY (produto_id) REFERENCES produto (id),
  CONSTRAINT FKeyouxfvoi291lpo5168e6wpej FOREIGN KEY (pedido_id) REFERENCES pedido (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;