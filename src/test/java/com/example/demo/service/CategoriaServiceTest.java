package com.example.demo.service;

import com.example.demo.domain.Categoria;
import com.example.demo.dto.CategoriaDTO;
import com.example.demo.repository.CategoriaRepository;
import com.example.demo.service.Exceptions.ObjectNotFoundException;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
public class CategoriaServiceTest {

    @Mock
    CategoriaRepository categoriaRepository;

    @InjectMocks
    CategoriaService categoriaService;

    public List<Categoria> catList;
    public Categoria cat;

    @Before
    public void mock(){
        catList = new ArrayList<>();
        Categoria categoria = new Categoria(1L,"categoria1");
        Categoria categoria2 = new Categoria(2L,"categoria2");
        catList.add(categoria);
        catList.add(categoria2);

        this.cat = new Categoria(1L,"categoria1");
    }

    @Test
    public void testLlistAll() {

       List<Categoria> tt =  categoriaService.listAll();
       when(categoriaRepository.findAll()).thenReturn(catList);

        assertThat(!tt.isEmpty());
    }


    @Test
    public void testFindById() {

        try {
            Categoria cat = categoriaService.findById(1L);
            when(categoriaService.findById(1L)).thenReturn(cat);

            assertThat(cat != null);
            assertThat(cat.getCategoria().equals("categoria1"));
        } catch (ObjectNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdate(){
        Categoria cats =  new Categoria(1L,"categoria1");
        categoriaService.update(cats);

        when(categoriaService.update(cats)).thenReturn(cats);


        assertThat(cats != null);
    }

    @Test
    public void testInsert() {

        Categoria cats =  new Categoria(1L,"categoria1");
        Categoria cat = categoriaService.insert(cats);

        when(categoriaService.insert(cats)).thenReturn(cat);

        assertThat(cats != null);

    }

    @Test
    public void testDelete(){


        assertThrows(ObjectNotFoundException.class, () -> categoriaService.delete(null));
    }


    @Test
    public void testConverter() {

        Categoria cats =  new Categoria(1L,"categoria1");
        CategoriaDTO dto = new CategoriaDTO(cats);
        categoriaService.convertDTO(dto);

        when(categoriaService.convertDTO(dto)).thenReturn(cats);

        assertThat(cats != null);
        assertThat(cats.getCategoria().equals(dto.getCategoria()));

    }




}
