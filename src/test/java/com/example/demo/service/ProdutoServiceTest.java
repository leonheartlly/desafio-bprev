package com.example.demo.service;

import com.example.demo.domain.Categoria;
import com.example.demo.domain.PedidoItem;
import com.example.demo.domain.Produto;
import com.example.demo.dto.CategoriaDTO;
import com.example.demo.dto.ProdutoDTO;
import com.example.demo.repository.ProdutoRepository;
import com.example.demo.service.Exceptions.ObjectNotFoundException;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
public class ProdutoServiceTest {

    @Mock
    ProdutoRepository produtoRepository;

    @InjectMocks
    ProdutoService produtoService;

    public List<Produto> catList;
    public Produto cat;

    @Before
    public void mock(){
        catList = new ArrayList<>();
        Set <PedidoItem> pei = new HashSet<PedidoItem>();
        Produto prod = new Produto(1L, "produto", 10D , 2L,  "descricao", "foto", 1L, pei);
        Produto prod2 = new Produto(2L, "produto2", 12D , 22L,  "descricao2", "foto2", 2L, pei);
        catList.add(prod);
        catList.add(prod2);

        this.cat = new Produto(1L, "produto", 10D , 2L,  "descricao", "foto", 1L, pei);
    }

    @Test
    public void testLlistAll() {

       List<Produto> tt =  produtoService.listAll();
       when(produtoRepository.findAll()).thenReturn(catList);

        assertThat(!tt.isEmpty());
    }


    @Test
    public void testFindById() {

        try {
            Produto cat = produtoService.findById(1L);
            when(produtoService.findById(1L)).thenReturn(cat);

            assertThat(cat != null);
            assertThat(cat.getDescricao().equals("categoria1"));
        } catch (ObjectNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdate(){
        Set <PedidoItem> pei = new HashSet<PedidoItem>();
        Produto cats =  new Produto(1L, "produto", 10D , 2L,  "descricao", "foto", 1L, pei);
        produtoService.update(cats);

        when(produtoService.update(cats)).thenReturn(cats);


        assertThat(cats != null);
    }

    @Test
    public void testInsert() {
        Set <PedidoItem> pei = new HashSet<PedidoItem>();
        Produto cats =  new Produto(1L, "produto", 10D , 2L,  "descricao", "foto", 1L, pei);
        Produto cat = produtoService.insert(cats);

        when(produtoService.insert(cats)).thenReturn(cat);

        assertThat(cats != null);

    }

    @Test
    public void testDelete(){

        
        assertThrows(ObjectNotFoundException.class, () -> produtoService.delete(null));
    }


    @Test
    public void testConverter() {
        Set <PedidoItem> pei = new HashSet<PedidoItem>();
        Produto prod =  new Produto(1L, "produto", 10D , 2L,  "descricao", "foto", 1L, pei);
        ProdutoDTO dto = new ProdutoDTO(prod);
        produtoService.convertDTO(dto);

        when(produtoService.convertDTO(dto)).thenReturn(prod);

        assertThat(prod != null);
        assertThat(prod.getDescricao().equals(dto.getDescricao()));

    }




}
